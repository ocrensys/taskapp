# TaskApp <img src="https://i.ibb.co/fYXJxyY/5847ea22cef1014c0b5e4833-2.png" alt="icon" width="40" style=" margin-bottom:-10px;"/> 

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) versión 16.1.0.

## Development

Ejecuta `ng serve` para correr tu servidor de desarrollo. Ingresa a `http://localhost:4200/`. La aplicación se ejecutara de manera inmediata y podrás interactuar con la *interface de usuario*.

## Build

Ejecuta `ng run build` para compilar y `ng run deploy` para hacer un despliegue con las configuraciones de tu proyecto en [Firebase Console](https://console.firebase.google.com/) .

## Demo <img src="https://icon-library.com/images/deploy-icon/deploy-icon-8.jpg" alt="icon" width="20"/>

Navega hacia [TaskApp Demo](https://taskapp-8ae9b.firebaseapp.com) para ingresar a la aplicación. TaskApp ha sido deplegada en [Firebase Hosting](https://firebase.google.com/docs/hosting) aprovechando que sus servidores entregan un excelente rendimiento, y ademas, viene con SSL integrado, lo que lo hace muy seguro.

<img src="https://i.ibb.co/kcBTV1J/list-page.jpg" alt="icon" style="filter: drop-shadow(1px 1px 10px #999999);"/>

## Descripción de la aplicación

Puedes realizar varias acciones con tus tareas, entre las que se encuantran las siguientes:

– [x] Crear una nueva tarea
– [x] Cambiar el estado a completado
– [x] Actualizar los datos de la tarea
– [x] Visualizar las tareas realizadas en una lista aparte
– [x] Visualizar los detalles de las taeas
– [x] Eliminar la tarea que ya no es necesaria

## Mas infomación

Para saber mas informnación te dejo el link de mi perfil en [LikedIn](https://www.linkedin.com/in/jairo-martinez-a14b94240/) asi compartir tus dudas y sugerencias y descubrir más acerca de este maravillo framework [Angular](https://angular.io/).